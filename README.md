# number_of_virions

This repository is accompanying the paper ["The total number and mass of SARS-CoV-2 virions in an infected person"](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7685332/)

Ron Sender*, Yinon M. Bar-On*, Shmuel Gleizer, Biana Bernsthein, Avi Flamholz, Rob Phillips, Ron Milo 

*equal contribution

corresponding aothur: RM ron.milo@weizmann.ac.il

This repository contain two jupyter notebooks and one xlsx file: 

* **[`JN1_RM_tissue_concentrations.ipynb`](./JN1_RM_tissue_concentrations.ipynb)|** code for summerazing the viral concentration in the different tissues, based on data extracted from studies of rhesus macaques 

* **[`merged_RM_data.xlsx`](./merged_RM_data.xlsx)|** curated data from the literature for estimating viral concentration of SARS-CoV-2 in different tissues

* **[`JN2_Uncertainty_LHA.ipynb`](./JN2_Uncertainty_LHA.ipynb)|** code for estimating the uncertainty in the analysis using the Latin Hypercube method
